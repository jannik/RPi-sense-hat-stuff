# sense-hat-stuff
*Little Python scripts for the Raspberry Pi Sense HAT*

![Badge](https://img.shields.io/badge/fun-7%2F10-yellow) ![Badge](https://img.shields.io/badge/awesomeness-6%2F10-orange) ![Badge](https://img.shields.io/badge/license-CC0-green)

---

These are some little Python sripts created for the awesome Sense HAT. Just a personal collection of self-written examples and funny bits. 

Feel free to fork and and have fun!

There are a few dependencies right now you have to fix yourself before trying to run the project.
For the moment please have a look at all imported modules in the code.
